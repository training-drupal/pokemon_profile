<?php

namespace Drupal\training_module\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\State\StateInterface;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a helper class for importing default content.
 *
 * @internal
 *  This code is only for use by the training profile, for creating content.
 */
class InstallHelper implements ContainerInjectionInterface {

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Enabled languages.
   *
   * List of all enabled languages.
   *
   * @var array
   */
  protected $enabledLanguages;

  /**
   * Media Image CSV ID map.
   *
   * Used to store media image CSV IDs created in the import process.
   * This allows the created media images to be cross referenced when creating
   * term and node.
   *
   * @var array
   */
  protected $mediaImageIdMap;
  /**
   * File Image CSV ID map.
   *
   * Used to store file image CSV IDs created in the import process.
   * This allows the created file images to be cross referenced when creating
   * user.
   *
   * @var array
   */
  protected $imagePictureIdMap;

  /**
   * Term ID map.
   *
   * Used to store term IDs created in the import process against
   * vocabulary and row in the source CSV files. This allows the created terms
   * to be cross referenced when creating node and user.
   *
   * @var array
   */
  protected $termIdMap;

  /**
   * User CSV ID map.
   *
   * Used to store node CSV IDs created in the import process. This allows the
   * created nodes to be cross referenced when creating blocks.
   *
   * @var array
   */
  protected $userIdMap;

  /**
   * Node CSV ID map.
   *
   * Used to store node CSV IDs created in the import process. This allows the
   * created nodes to be cross referenced when creating blocks.
   *
   * @var array
   */
  protected $nodeIdMap;

  /**
   * Menu link ID map.
   *
   * Used to store node CSV IDs created in the import process. This allows the
   * created nodes to be cross referenced when creating blocks.
   *
   * @var array
   */
  protected $menuLinkIdMap;

  /**
   * Constructs a new InstallHelper self.
   *
   * @param \Drupal\path_alias\AliasManagerInterface $aliasManager
   *   The path alias manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   */
  public function __construct(AliasManagerInterface $aliasManager, EntityTypeManagerInterface $entityTypeManager, ModuleHandlerInterface $moduleHandler, StateInterface $state, FileSystemInterface $fileSystem) {
    $this->aliasManager = $aliasManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->moduleHandler = $moduleHandler;
    $this->state = $state;
    $this->fileSystem = $fileSystem;
    $this->enabledLanguages = \array_keys(\Drupal::languageManager()->getLanguages());
    $this->imagePictureIdMap = [];
    $this->mediaImageIdMap = [];
    $this->termIdMap = [];
    $this->userIdMap = [];
    $this->nodeIdMap = [];
    $this->menuLinkIdMap = [];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('path_alias.manager'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('state'),
      $container->get('file_system')
    );
  }

  /**
   * Imports default contents.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function importContent() {
    $this->getModulePath()
      ->importContentFromFile('file', 'image')
      ->importContentFromFile('media', 'media')
      ->importContentFromFile('taxonomy_term', 'pokemon_type')
      ->importContentFromFile('taxonomy_term', 'pokemon_generation')
      ->importContentFromFile('taxonomy_term', 'news')
      ->importUserFromFile('account')
      ->importContentFromFile('node', 'page')
      ->importContentFromFile('node', 'article')
      ->importContentFromFile('node', 'pokemon')->updateContentTypeReferences('node', 'pokemon')
      ->createMenu();
  }

  /**
   * Deletes any content imported by this module.
   *
   * @return $this
   */
  public function deleteImportedContent() {
    $uuids = $this->state->get('training_content_uuids', []);
    $by_entity_type = \array_reduce(\array_keys($uuids), function ($carry, $uuid) use ($uuids) {
      $entity_type_id = $uuids[$uuid];
      $carry[$entity_type_id][] = $uuid;
      return $carry;
    }, []);
    foreach ($by_entity_type as $entity_type_id => $entity_uuids) {
      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      $entities = $storage->loadByProperties(['uuid' => $entity_uuids]);
      $storage->delete($entities);
    }
    return $this;
  }

  /**
   * Set module_path variable.
   *
   * @return $this
   */
  protected function getModulePath() {
    $this->module_path = $this->moduleHandler->getModule('training_module')->getPath();
    return $this;
  }

  /**
   * Imports content.
   *
   * @param string $entity_type
   *   Entity type to be imported.
   * @param string $bundle_machine_name
   *   Bundle machine name to be imported.
   *
   * @return $this
   */
  protected function importContentFromFile(string $entity_type, string $bundle_machine_name) {
    $filename = $entity_type . '/' . $bundle_machine_name . '.csv';

    // Read all multilingual content from the file.
    list($all_content, $translated_languages) = $this->readMultilingualContent($filename);

    // Start wtih default language content.
    // English is no longer needed in the list of languages to translate.
    $key = \array_search('en', $translated_languages);
    unset($translated_languages[$key]);
    foreach ($all_content['en'] as $current_content) {
      // Process data into its relevant structure.
      $structured_content = $this->processContent($entity_type, $bundle_machine_name, $current_content, 'en');
      // Save Entity.
      $entity = $this->entityTypeManager->getStorage($entity_type)->create($structured_content);
      $entity->save();
      $this->storeCreatedContentUuids([$entity->uuid() => $entity_type]);

      // Save taxonomy entity Drupal ID, so we can reference it in nodes.
      if ($entity_type == 'taxonomy_term') {
        $this->saveTermId($bundle_machine_name, $current_content['id'], $entity->id());
      }

      // Save media entity Drupal ID, so we can reference it in nodes & blocks.
      if ($entity_type == 'file') {
        $this->saveFileImageId($current_content['id'], $entity->id());
      }

      // Save media entity Drupal ID, so we can reference it in nodes & blocks.
      if ($entity_type == 'media') {
        $this->saveMediaImageId($current_content['id'], $entity->id());
      }

      // Save node ID, so we can reference it in nodes & blocks.
      if ($entity_type == 'node') {
        $this->saveNodeId($bundle_machine_name, $current_content['id'], $entity->id());
      }

      // Go through all the languages that have translations.
      // Term, Node, block_content.
      foreach ($translated_languages as $translated_language) {

        // Find the translated content ID that corresponds to original content.
        $translation_id = \array_search($current_content['id'], \array_column($all_content[$translated_language], 'id'));

        // Check if translation was found.
        if ($translation_id !== FALSE) {

          // Process that translation.
          $translated_entity = $all_content[$translated_language][$translation_id];
          $structured_content = $this->processContent($entity_type, $bundle_machine_name, $translated_entity, $translated_language);

          // Save entity's translation.
          $entity->addTranslation(
            $translated_language,
            $structured_content
          );
          $entity->save();
        }
      }
    }
    return $this;
  }

  /**
   * Imports content.
   *
   * @param string $entity_type
   *   Entity type to be imported.
   * @param string $bundle_machine_name
   *   Bundle machine name to be imported.
   *
   * @return $this
   */
  protected function updateContentTypeReferences(string $entity_type, $bundle_machine_name) {
    $filename = $entity_type . '/references/' . $bundle_machine_name . '.csv';
    // Read all multilingual content from the file.
    list($all_content, $translated_languages) = $this->readMultilingualContent($filename);
    foreach ($all_content['en'] as $current_content) {
      // Process data into its relevant structure.
      $structured_content = $this->processUpdate($entity_type, $bundle_machine_name, $current_content, 'en');
      $current_node_id = $this->getNodeId($bundle_machine_name, $current_content['id']);
      // Save Entity.
      $entity = $this->entityTypeManager->getStorage($entity_type)->load($current_node_id);

      foreach ($structured_content as $field => $value) {
        $entity->set($field, $value);
      }
      $entity->save();
    }
    return $this;
  }

  /**
   * Imports users.
   *
   * @param string $filename
   *   Filename of the account data file to be imported.
   *
   * @return $this
   */
  protected function importUserFromFile(string $filename) {
    $filename = 'user/' . $filename . '.csv';

    // Read all multilingual content from the file.
    list($all_content, $translated_languages) = $this->readMultilingualContent($filename);

    foreach ($all_content['en'] as $content) {
      // Process data into its relevant structure.
      $this->processUser($content);
    }
    return $this;
  }

  /**
   * Read multilingual content.
   *
   * @param string $filename
   *   Filename to import.
   *
   * @return array
   *   An array of two items:
   *     1. All multilingual content that was read from the files.
   *     2. List of language codes that need to be imported.
   */
  protected function readMultilingualContent(string $filename) {
    $default_content_path = $this->module_path . "/default_content/entities/";
    // Get all enabled languages.
    $translated_languages = $this->enabledLanguages;
    // Load all the content from any CSV files that exist for enabled languages.
    foreach ($translated_languages as $language) {
      if (\file_exists($default_content_path . "$language/$filename") &&
      ($handle = \fopen($default_content_path . "$language/$filename", 'r')) !== FALSE) {
        $header = \fgetcsv($handle);
        $line_counter = 0;
        while (($content = \fgetcsv($handle)) !== FALSE) {
          $keyed_content[$language][$line_counter] = \array_combine($header, $content);
          $line_counter++;
        }
        \fclose($handle);
      }
      else {
        // Language directory exists, but file in this language was not found.
        // Remove that language from list of languages to be translated.
        $key = \array_search($language, $translated_languages);
        unset($translated_languages[$key]);
      }
    }
    return [$keyed_content, $translated_languages];
  }

  /**
   * Process content into a structure that can be saved into Drupal.
   *
   * @param string $entity_type
   *   Current Entity type.
   * @param string $bundle_machine_name
   *   Current bundle's machine name.
   * @param array $content
   *   Current content array that needs to be structured.
   * @param string $langcode
   *   Current language code.
   *
   * @return array
   *   Structured content.
   */
  protected function processContent(string $entity_type, string $bundle_machine_name, array $content, string $langcode) {
    switch ($bundle_machine_name) {
      case 'image':
        $structured_content = $this->processImage($content);
        break;

      case 'media':
        $structured_content = $this->processMedia($content);
        break;

      case 'pokemon_type':
      case 'pokemon_generation':
      case 'news':
        $structured_content = $this->processTerm($entity_type, $bundle_machine_name, $content, $langcode);
        break;

      case 'article':
      case 'pokemon':
      case 'page':
        $structured_content = $this->processNode($entity_type, $bundle_machine_name, $content, $langcode);
        break;

      case 'brand_information':
      case 'contact':
        $structured_content = $this->processBlock($entity_type, $bundle_machine_name, $content, $langcode);
        break;

      default:
        break;
    }
    return $structured_content;
  }

  /**
   * Process content into a structure that can be saved into Drupal.
   *
   * @param string $entity_type
   *   Current Entity type.
   * @param string $bundle_machine_name
   *   Current bundle's machine name.
   * @param array $content
   *   Current content array that needs to be structured.
   * @param string $langcode
   *   Current language code.
   *
   * @return array
   *   Structured content.
   */
  protected function processUpdate(string $entity_type, string $bundle_machine_name, array $content, string $langcode) {
    switch ($bundle_machine_name) {
      case 'pokemon':
        $structured_content = $this->updateNode($entity_type, $bundle_machine_name, $content, $langcode);
        break;

      default:
        break;
    }
    return $structured_content;
  }

  /**
   * Process images into media entities.
   *
   * @param array $data
   *   Data of line that was read from the file.
   *
   * @return array
   *   Data structured as a image.
   */
  protected function processImage(array $data) {
    $directory = 'public://' . \trim($data['folder']);
    $realpath = $this->fileSystem->realpath($directory);
    $filename = \trim($data['filename']);
    $image_path = $this->module_path . '/default_content/datas/medias/' . $filename;

    if (!file_exists($realpath)) {
      $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    }

    try {
      $uri = $this->fileSystem->copy($image_path, $directory . '/' . $filename, FileSystemInterface::EXISTS_REPLACE);
    }
    catch (FileException $e) {
      $uri = FALSE;
    }
    // Prepare content.
    $values = [
      'uri' => $uri,
      'filename' => $filename,
      'status' => 1,
    ];
    return $values;
  }

  /**
   * Process images into media entities.
   *
   * @param array $data
   *   Data of line that was read from the file.
   *
   * @return array
   *   Data structured as a image.
   */
  protected function processMedia(array $data) {

    $image_path = $this->module_path . '/default_content/datas/medias/' . $data['filename'];
    // Prepare content.
    $values = [
      'name' => $data['title'],
      'bundle' => 'image',
      'langcode' => 'en',
      'field_media_image' => [
        'target_id' => $this->createFileEntity($image_path, \trim($data['folder'])),
        'alt' => $data['alt'],
      ],
    ];
    return $values;
  }

  /**
   * Creates a file entity based on an image path.
   *
   * @param string $path
   *   Image path.
   * @param string $folder
   *   Folder name.
   *
   * @return int
   *   File ID.
   */
  protected function createFileEntity(string $path, string $folder) {
    $filename = basename($path);
    try {
      $uri = $this->fileSystem->copy($path, 'public://' . $folder . '/' . $filename, FileSystemInterface::EXISTS_REPLACE);
    }
    catch (FileException $e) {
      $uri = FALSE;
    }
    $file = $this->entityTypeManager->getStorage('file')->create([
      'uri' => $uri,
      'status' => 1,
    ]);
    $file->save();
    $this->storeCreatedContentUuids([$file->uuid() => 'file']);
    return $file->id();
  }

  /**
   * Process user and create account.
   *
   * @param array $data
   *   Data of line that was read from the file.
   *
   * @return $this
   */
  protected function processUser(array $data) {
    // @todo Fix this script to be compatible with the content processing method.
    $user = $this->entityTypeManager->getStorage('user')->create();
    $username = \trim($data['username']);
    $mail = \mb_strtolower(\str_replace(' ', '.', $username)) . '@jvgame.org';

    $user->setUsername($username);
    $user->setEmail($mail);

    if (isset($data['role']) && !empty($data['role'])) {
      $user->addRole(\trim($data['role']));
    }

    // Set custom basic fields value if exists.
    $fields = [];
    $fields = $this->filterFields('field', $data);
    if (!empty($fields)) {
      foreach ($fields as $field => $field_value) {
        $user->set($field, \trim($field_value));
      }
    }

    // Set custom reference fields value if exists.
    $references = [];
    $references = $this->filterFields('reference', $data);
    if (!empty($references)) {
      foreach ($references as $reference => $reference_value) {
        $elements = \explode('-', $reference);
        $entity = $elements[1];
        $field_name = $elements[2];

        switch ($entity) {
          case 'image':
            $values = [];
            if ($tid = $this->getFileImageId($reference_value)) {
              $values[] = ['target_id' => $tid];
            }
            $user->set($field_name, $values);
            break;

          default:
            $values = [];
            $tids = \array_filter(\explode(',', $reference_value));
            foreach ($tids as $tag_id) {
              if ($tid = $this->getTermId($entity, $tag_id)) {
                $values[] = ['target_id' => $tid];
              }
            }
            $user->set($field_name, $values);
            break;
        }
      }
    }

    $user->enforceIsNew();
    $user->save();
    $this->storeCreatedContentUuids([$user->uuid() => 'user']);

    return $this;
  }

  /**
   * Process terms for a given vocabulary and filename.
   *
   * @param string $entity_type
   *   Machine name of vocabulary to which we should save terms.
   * @param string $vocabulary
   *   Machine name of vocabulary to which we should save terms.
   * @param array $data
   *   Data of line that was read from the file.
   * @param string $langcode
   *   Machine name of vocabulary to which we should save terms.
   *
   * @return array
   *   Data structured as a term.
   */
  protected function processTerm(string $entity_type, string $vocabulary, array $data, string $langcode) {
    // Prepare content.
    $term_name = \trim($data['term']);
    $values = [
      'name' => $term_name,
      'description' => '',
      'vid' => $vocabulary,
      'path' => [
        'alias' => '/' . Html::getClass($vocabulary) . '/' . Html::getClass($term_name),
      ],
      'langcode' => $langcode,
    ];

    $this->getMetadataValue($data, $values, $langcode);
    $this->getHtmlFieldsValue($entity_type, $vocabulary, $data, $values, $langcode);
    $this->getLinkFieldsValue($data, $values);
    $this->getBasicFieldsValue($data, $values);
    $this->getReferenceFieldsValue($vocabulary, $data, $values);

    return $values;
  }

  /**
   * Process article data into article node structure.
   *
   * @param string $entity_type
   *   Current Entity type machine name.
   * @param string $bundle_machine_name
   *   Current bundle type machine name.
   * @param array $data
   *   Data of line that was read from the file.
   * @param string $langcode
   *   Current language code.
   *
   * @return array
   *   Data structured as an article node.
   */
  protected function processNode(string $entity_type, string $bundle_machine_name, array $data, string $langcode) {
    // Prepare content.
    $values = [
      'type' => $bundle_machine_name,
      'title' => $data['title'],
      'moderation_state' => 'published',
      'langcode' => $langcode,
    ];

    $this->getMetadataValue($data, $values, $bundle_machine_name);
    $this->getHtmlFieldsValue($entity_type, $bundle_machine_name, $data, $values, $langcode);
    $this->getLinkFieldsValue($data, $values);
    $this->getBasicFieldsValue($data, $values);
    $this->getReferenceFieldsValue($bundle_machine_name, $data, $values);

    return $values;
  }

  /**
   * Process article data into article node structure.
   *
   * @param string $entity_type
   *   Current Entity type machine name.
   * @param string $bundle_machine_name
   *   Current bundle type machine name.
   * @param array $data
   *   Data of line that was read from the file.
   * @param string $langcode
   *   Current language code.
   *
   * @return array
   *   Data structured as an article node.
   */
  protected function updateNode(string $entity_type, string $bundle_machine_name, array $data, string $langcode) {
    $values = [];
    $this->getReferenceFieldsValue($bundle_machine_name, $data, $values);
    return $values;
  }

  /**
   * Process block data into block content structure.
   *
   * @param string $entity_type
   *   Current Entity type machine name.
   * @param string $bundle_machine_name
   *   Current bundle type machine name.
   * @param array $data
   *   Data of line that was read from the file.
   * @param string $langcode
   *   Current language code.
   *
   * @return array
   *   Data structured as a block.
   */
  protected function processBlock(string $entity_type, string $bundle_machine_name, array $data, string $langcode) {
    // Prepare content.
    $values = [
      'uuid' => $data['uuid'],
      'info' => $data['info'],
      'type' => $bundle_machine_name,
      'langcode' => $langcode,
    ];

    $this->getHtmlFieldsValue($entity_type, $bundle_machine_name, $data, $values, $langcode);
    $this->getLinkFieldsValue($data, $values);
    $this->getBasicFieldsValue($data, $values);
    $this->getReferenceFieldsValue($bundle_machine_name, $data, $values);

    return $values;
  }

  /**
   * Create menu links entries.
   *
   * @return $this
   */
  public function createMenu() {
    $menu_links_uuids = [];
    $items = [
      'Pokemon official website' => 'https://www.pokemon.com/',
      'Pokemon API' => 'https://pokeapi.co/',
      'Nintendo Pokemon World' => 'https://www.nintendo.fr/Divers/Nintendo-Kids/Read-Discover/Bienvenue-dans-le-monde-des-Pokemon--1526891.html',
    ];
    foreach ($items as $title => $link) {
      $menu_link = \Drupal::entityTypeManager()
        ->getStorage('menu_link_content')
        ->create([
          'title' => $title,
          'link' => ['uri' => $link],
          'menu_name' => 'footer',
          'expanded' => TRUE,
        ]);
      $menu_link->save();
      $this->storeCreatedContentUuids([$menu_link->uuid() => 'menu_link_content']);
    }
    return $this;
  }

  /**
   * Get all metadatas content.
   *
   * @param array $data
   *   Data of line that was read from the file.
   * @param array $values
   *   The structured data of the entity.
   * @param string $bundle_machine_name
   *   Current bundle type machine name.
   */
  protected function getMetadataValue(array $data, array &$values, string $bundle_machine_name) {
    // Set article author if exist.
    if (isset($data['author']) && !empty($data['author'])) {
      $values['uid'] = $this->getUser($data['author']);
    }

    // Set node alias if exists.
    if (isset($data['slug']) && !empty($data['slug'])) {
      $values['path'] = [['alias' => '/' . $bundle_machine_name . '/' . $data['slug']]];
    }
  }

  /**
   * Get the value of all html type fields.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle_machine_name
   *   Current bundle's machine name.
   * @param array $data
   *   Data of line that was read from the file.
   * @param array $values
   *   The structured data of the entity.
   * @param string $langcode
   *   Current language code.
   */
  protected function getHtmlFieldsValue(string $entity_type, string $bundle_machine_name, array $data, array &$values, string $langcode) {
    $contents = $this->filterFields('html', $data);
    if (!empty($contents)) {
      foreach ($contents as $content => $filename) {
        $elements = \explode('-', $content);
        $field_name = $elements[1];

        $path = $this->module_path . '/default_content/datas/' . $langcode . '/' . $entity_type . '/' . $bundle_machine_name . '/' . $filename;
        if (\file_exists($path)) {
          $value = \file_get_contents($path);
          if ($value !== FALSE) {
            if ($field_name == 'description') {
              $values[$field_name] = $value;
            }
            else {
              $values[$field_name] = [
                [
                  'value' => $value,
                  'format' => 'basic_html',
                ],
              ];
            }
          }
        }
      }
    }
  }

  /**
   * Get the value of all link fields, whatever their cardinality.
   *
   * @param array $data
   *   Data of line that was read from the file.
   * @param array $values
   *   The structured data of the entity.
   */
  protected function getLinkFieldsValue(array $data, array &$values) {
    $links = $this->filterFields('link', $data);
    if (!empty($links)) {
      foreach ($links as $reference => $reference_value) {
        $elements = \explode('-', $reference);
        $field_name = $elements[1];
        $contents = \array_filter(\explode('-', $reference_value));
        foreach ($contents as $link) {
          $href = \explode(',', $link);
          $values[$field_name] = [
            'title' => $href[0],
            'uri' => $href[1],
          ];
        }
      }
    }
  }

  /**
   * Get the value of all basic fields, whatever their cardinality.
   *
   * @param array $data
   *   Data of line that was read from the file.
   * @param array $values
   *   The structured data of the entity.
   */
  protected function getBasicFieldsValue(array $data, array &$values) {
    $fields = $this->filterFields('field', $data);
    if (!empty($fields)) {
      foreach ($fields as $field_name => $inputs) {
        $inputs = \trim($inputs);
        $elements = \explode(',', $inputs);
        foreach ($elements as $input) {
          $values[$field_name][] = ['value' => $input];
        }
      }
    }
  }

  /**
   * Get the value of all reference type fields, whatever their cardinality.
   *
   * @param string $bundle_machine_name
   *   Current bundle machine name.
   * @param array $data
   *   Data of line that was read from the file.
   * @param array $values
   *   The structured data of the entity.
   */
  protected function getReferenceFieldsValue(string $bundle_machine_name, array $data, array &$values) {
    $references = $this->filterFields('reference', $data);
    if (!empty($references)) {
      foreach ($references as $reference => $reference_value) {
        $elements = \explode('-', $reference);
        $entity = $elements[1];
        $field_name = $elements[2];
        $ids = \array_filter(\explode(',', $reference_value));

        switch ($entity) {
          case 'media':
            foreach ($ids as $media_id) {
              if ($tid = $this->getMediaImageId($media_id)) {
                $values[$field_name][] = ['target_id' => $tid];
              }
            }
            break;

          case 'node':
            foreach ($ids as $node_id) {
              if ($nid = $this->getNodeId($bundle_machine_name, $node_id)) {
                $values[$field_name][] = ['target_id' => $nid];
              }
            }
            break;

          default:
            foreach ($ids as $tag_id) {
              if ($tid = $this->getTermId($entity, $tag_id)) {
                $values[$field_name][] = ['target_id' => $tid];
              }
            }
        }
      }
    }
  }

  /**
   * Callback function for filtering datas in the imported file.
   *
   * @param string $pattern
   *   The regex pattern.
   * @param array $data
   *   Data of line that was read from the file.
   *
   * @return array
   *   The filtrered datas.
   */
  protected function filterFields(string $pattern, array $data) {
    switch ($pattern) {
      case 'html':
        $fields = \array_filter($data, function ($k) {
          return \preg_match('/^html-.+$/', $k);
        }, ARRAY_FILTER_USE_KEY);
        break;

      case 'reference':
        $fields = \array_filter($data, function ($k) {
          return \preg_match('/^reference-.+$/', $k);
        }, ARRAY_FILTER_USE_KEY);
        break;

      case 'link':
        $fields = \array_filter($data, function ($k) {
          return \preg_match('/^link-.+$/', $k);
        }, ARRAY_FILTER_USE_KEY);
        break;

      case 'field':
        $fields = \array_filter($data, function ($k) {
          return \preg_match('/^field_.+$/', $k);
        }, ARRAY_FILTER_USE_KEY);
        break;

      default:
        $fields = [];
    }
    return $fields;
  }

  /**
   * Retrieves the File ID of a file saved during the import process.
   *
   * @param int $file_image_csv_id
   *   The File image's ID from the CSV file.
   *
   * @return int
   *   File Image ID, or 0 if File Image ID could not be found.
   */
  protected function getFileImageId($file_image_csv_id) {
    if (\array_key_exists($file_image_csv_id, $this->imagePictureIdMap)) {
      return $this->imagePictureIdMap[$file_image_csv_id];
    }
    return 0;
  }

  /**
   * Saves a File ID generated when saving a File image.
   *
   * @param int $file_image_csv_id
   *   The File image's ID from the CSV file.
   * @param int $image_picture_id
   *   File Image ID generated when saved in the Drupal database.
   */
  protected function saveFileImageId($file_image_csv_id, $image_picture_id) {
    $this->imagePictureIdMap[$file_image_csv_id] = $image_picture_id;
  }

  /**
   * Retrieves the Media Image ID of a media image saved during the import.
   *
   * @param int $media_image_csv_id
   *   The media image's ID from the CSV file.
   *
   * @return int
   *   Media Image ID, or 0 if Media Image ID could not be found.
   */
  protected function getMediaImageId($media_image_csv_id) {
    if (\array_key_exists($media_image_csv_id, $this->mediaImageIdMap)) {
      return $this->mediaImageIdMap[$media_image_csv_id];
    }
    return 0;
  }

  /**
   * Saves a Media Image ID generated when saving a media image.
   *
   * @param int $media_image_csv_id
   *   The media image's ID from the CSV file.
   * @param int $media_image_id
   *   Media Image ID generated when saved in the Drupal database.
   */
  protected function saveMediaImageId($media_image_csv_id, $media_image_id) {
    $this->mediaImageIdMap[$media_image_csv_id] = $media_image_id;
  }

  /**
   * Retrieves the Term ID of a term saved during the import process.
   *
   * @param string $vocabulary
   *   Machine name of vocabulary to which it was saved.
   * @param int $term_csv_id
   *   The term's ID from the CSV file.
   *
   * @return int
   *   Term ID, or 0 if Term ID could not be found.
   */
  protected function getTermId($vocabulary, $term_csv_id) {
    if (\array_key_exists($vocabulary, $this->termIdMap) &&
        \array_key_exists($term_csv_id, $this->termIdMap[$vocabulary])) {
      return $this->termIdMap[$vocabulary][$term_csv_id];
    }
    return 0;
  }

  /**
   * Saves a Term ID generated when saving a taxonomy term.
   *
   * @param string $vocabulary
   *   Machine name of vocabulary to which it was saved.
   * @param int $term_csv_id
   *   The term's ID from the CSV file.
   * @param int $tid
   *   Term ID generated when saved in the Drupal database.
   */
  protected function saveTermId($vocabulary, $term_csv_id, $tid) {
    $this->termIdMap[$vocabulary][$term_csv_id] = $tid;
  }

  /**
   * Retrieves the node path of node CSV ID saved during the import process.
   *
   * @param string $bundle_machine_name
   *   Current bundle's machine name.
   * @param string $node_csv_id
   *   The node's ID from the CSV file.
   *
   * @return string
   *   Node id, or 0 if node CSV ID could not be found.
   */
  protected function getNodeId(string $bundle_machine_name, string $node_csv_id) {
    if (\array_key_exists($bundle_machine_name, $this->nodeIdMap) &&
        \array_key_exists($node_csv_id, $this->nodeIdMap[$bundle_machine_name])) {
      return $this->nodeIdMap[$bundle_machine_name][$node_csv_id];
    }
    return 0;
  }

  /**
   * Saves a node CSV ID generated when saving content.
   *
   * @param string $bundle_machine_name
   *   Current bundle's machine name.
   * @param string $node_csv_id
   *   The node's ID from the CSV file.
   * @param int $node_id
   *   Node's URL alias when saved in the Drupal database.
   */
  protected function saveNodeId(string $bundle_machine_name, string $node_csv_id, int $node_id) {
    $this->nodeIdMap[$bundle_machine_name][$node_csv_id] = $node_id;
  }

  /**
   * Saves a node CSV ID generated when saving content.
   *
   * @param string $menu_name
   *   The name of the menu.
   * @param int $menu_link_id
   *   Node's URL alias when saved in the Drupal database.
   */
  protected function saveMenuLinkId(string $menu_name, int $menu_link_id) {
    $this->menuLinkIdMap[$menu_name][] = $menu_link_id;
  }

  /**
   * Looks up a user by name, return admin if null.
   *
   * @param string $name
   *   Username.
   *
   * @return int
   *   User ID.
   */
  protected function getUser(string $name) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $users = $user_storage->loadByProperties(['name' => $name]);
    if (!empty($users)) {
      $user = reset($users);
      return $user->id();
    }
    return 1;
  }

  /**
   * Stores record of content entities created by this import.
   *
   * @param array $uuids
   *   Array of UUIDs where the key is the UUID and the value is the entity
   *   type.
   */
  protected function storeCreatedContentUuids(array $uuids) {
    $uuids = $this->state->get('training_content_uuids', []) + $uuids;
    $this->state->set('training_content_uuids', $uuids);
  }

}
